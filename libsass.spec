Name:           libsass
Version:        3.6.6
Release:        1
Summary:        A Sass CSS precompiler which is ported for C/C++

License:        MIT
URL:            http://sass-lang.com/libsass
Source0:        https://github.com/sass/libsass/archive/libsass-%{version}.tar.gz

BuildRequires:  automake autoconf libtool pkgconfig gcc-c++

%description
Libsass is a Sass CSS precompiler which is ported for C/C++. This version is
more efficient and portable than the original Ruby version. Keeping light and
sample is its degisn philosophy which makes it more easier to be built and integrated
with a immense amount of platforms and languages. Installation of saccs is needed
if you want to run is directly as libsass is just a library.

%package        devel
Summary:        Library and header files for libsass
Requires:       libsass = %{version}-%{release}

%description    devel
The libsass-devel package contains libraries and header files for
developing applications using libsass.

%prep
%autosetup -p1
export LIBSASS_VERSION=%{version}
autoreconf --force --install

%build
%configure --disable-static
%make_build

%install
%make_install
%delete_la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license LICENSE
%doc Readme.md SECURITY.md
%{_libdir}/*.so.*

%files          devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Wed Jul 31 2024 dillon chen <dillon.chen@gmail.com> - 3.6.6-1
- Update to 3.6.6

* Tue Dec 26 2023 liningjie <liningjie@xfusion.com> - 3.6.5-2
- Fix CVE-2022-26592 CVE-2022-43358 CVE-2022-43357

* Fri Jul 07 2023 wangkai <13474090681@163.com> - 3.6.5-1
- Update to 3.6.5

* Mon Dec 7 2020 wutao <wutao61@huawei.com> - 3.6.4-1
- update to 3.6.4 to fix use of unintialized problem

* Tue Dec 31 2019 lihao <lihao129@huawei.com> - 3.5.4-4
- Package Init

